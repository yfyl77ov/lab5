import unittest
import prac4


class ProgTest(unittest.TestCase):

    
    def test_1(self):
        '''
        Тест функции rez при вводе числа 15
        '''
        self.assertEqual(prac4.rez(15), 6)


    def test_2(self):
        '''
        Тест функции rez при вводе числа 17
        '''
        self.assertEqual(prac4.rez(17), 7)


    def test_3(self):
        '''
        Тест функции rez при вводе числа 16
        '''
        self.assertEqual(prac4.rez(16), 0)



if __name__ == '__main__':
    unittest.main()
