''' Филатов Ярослав Вариант 15 КИ19-17/2Б '''


def checkk():
    '''
    Функция проверяет вводимое число на условия(ничего не принимает)
    Args:
        Ничего.

    Returns:
        Число,если оно удовлетворяет условию.

    Raises:
        ValueError.

    Examples:
        >>>checkk()
        >>>5
        5
        >>>checkk()
        >>>3
        ValueErorr: Incorrect entry. Try again.
    '''
    while True:
        try:
            n = int(input('Enter positive number greater than 4: '))
            if n < 5:
                raise ValueError
            break
        except ValueError:
            print('Incorrect entry. Try again.')
    return n


def rez(n):
    '''
    Функция проверяет вводимое число на четность
    Args:
        arg1:  Число n.

    Returns:
        n/2 - 1,если n удовлетворяет условию. Иначе 0.

    Examples:
        >>>rez(15)
        6
        >>>rez(16)
        0
    '''
    if n % 2 != 0:
        result = int(n/2) - 1
    else:
        result = 0
    return result

if __name__ == '__main__':
    result = rez(checkk())
    if result != 0:
        print(1, result)
    else:
        print('Nope')
