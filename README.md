# Создание программы вычисляющей корни уравнения

## Задание

Дана некоторая преобразующая функция от двух аргументов F(x,y)= x^2+2xy+x+1 
и некоторое число N, причём x, y, N – целые положительные числа. Необходимо 
найти такую пару чисел x и y, чтобы F(x,y) = N.
Примечание: если таких пар несколько, выбрать ту, в которой x меньше

## Этапы

1. **Написана программа согласно заданию из 4 практической.**
    * *Написана функция проверки ввода*
        ```python
        def checkk():
            while True:
                try:
                    n = int(input('Enter positive number greater than 4: '))
                    if n < 5:
                        raise ValueError
                    break
                except ValueError:
                    print('Incorrect entry. Try again.')
            return n
        ```
    * *Алгоритм реализации задания*
    
        Аргумент x придется всегда брать за еденицу, а y необходимо проверять на
        четность и затем вычислять по формуле (1):
        
        $`result = int(n/2) - 1`$ (1)
    * *Написана функция выполняющая задание*
        ```python
        def rez(n):
            if n % 2 != 0:
                result = int(n/2) - 1
            else:
                result = 0
        return result
        ```
    * * Основная часть программы выглядит так*
        ```python
        if __name__ == '__main__':
            result = rez(checkk())
            if result != 0:
                print(1, result)
            else:
                print('Nope')
        ```
    
    * *Написаны тесты*
        ```python
        import unittest
        import prac4


        class ProgTest(unittest.TestCase):
        
            
            def test_1(self):
                self.assertEqual(prac4.rez(15), 6)
        
        
            def test_2(self):
                self.assertEqual(prac4.rez(17), 7)
        
        
            def test_3(self):
                self.assertEqual(prac4.rez(16), 0)
        
        
        if __name__ == '__main__':
            unittest.main()
        ```
2. **Написана документация** 

*Пример с функцией вып. задание:* 

```python
    """
    Функция проверяет вводимое число на четность
Args:
    arg1:  Число n.

Returns:
    n/2 - 1,если n удовлетворяет условию. Иначе 0.

Examples:
    >>>rez(15)
    6
    >>>rez(16)
    0
    """
```
3. **Долго мучаемся с Doxygen**

![](https://sun9-5.userapi.com/c854124/v854124291/1965f6/fZjW8zWou54.jpg)

4. **Редактируем README.md воспользовавшись [помощью](https://vk.com/away.php?to=https%3A%2F%2Fabout.gitlab.com%2Fhandbook%2Fproduct%2Ftechnical-writing%2Fmarkdown-guide%2F&cc_key=)**

5. **Готово!**
